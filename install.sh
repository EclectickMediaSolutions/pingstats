#! /bin/bash

git clone https://gitlab.com/EclectickMediaSolutions/pingstats.git
cd pingstats && git submodule init && git submodule update 
echo "Running pip..." && pip3 install .
echo "Removing source directory" && cd ../ && rm -rf pingstats
