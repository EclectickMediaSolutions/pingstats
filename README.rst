Pingstats, a simple CLI based ping visualization script
=======================================================

This script provides a very simple CLI based ping visualization script by utilizing `hipster plot`_.

This project is a much simplified version of PingStats_, a project of mine that went from being useful (i.e, the functionality of this script) to an over complicated mess of spaghetti code. That software provides GUI based plotting, as well as CSV based logging of ping data over time.

Installation
============

Installation has been made easy on any Unix based system that implements ``pip3``.
::

  pip3 install pingstats

.. note:: For versions previous to V0.4.3, manual install must be used. All versions aside from V0.1 can be installed by cloning the repository and recreating the install script manually.

Usage
=====

This software was implemented with simplicity in mind, and only provides one point of access:
::

  pingstats google.ca

Examples of software in use
===========================

.. image:: http://i64.tinypic.com/33mv6ud.png


In this image, we can see two separate outputs. The top display is a display of the most recent actual return times, whereas the bottom display is the average return time for each sequence.

This display automatically scales to whatever window you have open, adding more lines and columns as necessary.


Running the tests
=================

To run the tests, clone the repository:
::

  git clone https://gitlab.com/eclectickmediasolutions/pingstats.git

Then simply run:
::

  python3 setup.py test


Building the Docs
=================

To build the docs, first clone the repository:
::

  git clone https://gitlab.com/eclectickmediasolutions/pingstats.git

Then simply run:
::

  cd docs && make html

.. note:: The docs are built on top of sphinx, which can be installed via
          ``pip3 install sphinx``

The documentation will be generated in the path ``docs/_build/html/``. Open
``index.html`` to load the docs.


Contributing to the Project
===========================

The project uses a `git-native forking workflow`_. For example,

Clone the repository,
::

  git clone https://gitlab.com/eclectickmediasolutions/pingstats.git

Then, make your changes (in a branch), and push them to a new (public) remote:
::

  git remote set-url upstream $(git remote get-url origin)  # save origin as upstream
  git remote set-url origin git@your.new:path/to/repo
  git push -u origin master

Once your code is hosted publicly, notify the maintainers of your changes with a
``git request-pull``...
::

  git request-pull upstream origin  # upstream (this repo) should pull changes from new origin (new fork)

Our Issue Board
---------------

We use a decentralized/distributed bug tracking solution called `git bug`_
internally which can be viewed at any time by cloning the repository and running
``git bug pull``. 

Currently, this board is read only. If you encounter an issue, please contact
`the maintainers`_.

.. _`hipster plot`: https://github.com/imh/hipsterplot
.. _PingStats: https://github.com/eclectickmedia/pingstats
.. _`git-native forking workflow`: https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow
.. _`the maintainers`: mailto:ariana.giroux@gmail.com?Subject=Pingstats
.. _`git bug`: https://github.com/MichaelMure/git-bug
