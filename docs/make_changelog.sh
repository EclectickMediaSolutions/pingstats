#! /bin/bash

git --no-pager log --pretty="%d %s" --no-color "$1"~1.."$2" --reverse | sed 's/^/ -/'
