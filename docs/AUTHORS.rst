.. Outlines and logs all authors

.. toctree::
   :maxdepth: 2

.. _authors:

Authors
=======

Outlines all author details, including contributed versions.

====

.. codeauthor: Ariana Giroux <ariana.giroux@gmail.com>

:Author Name: Ariana Giroux
:Author Email: ariana.giroux@gmail.com
:Author Website: arianagiroux.ca
:Author Twitter: @ArianaGiroux

:Contributed versions:
   #. V0.01
   #. V0.1
   #. V0.2
   #. V0.3
   #. V0.3.1
   #. V0.3.2
   #. V0.3.3
   #. V0.3.4
   #. V0.3.5
   #. V0.4
   #. V0.4.1
   #. V0.4.2
   #. V0.4.3
   #. V0.5a1
   #. V0.5b1
   #. V0.5b2
   #. V0.5b3
   #. V0.5
   #. V0.6
   #. V0.6.1
   #. V0.6.2
   #. V0.7
   #. V0.8
   #. V0.8.1
   #. V0.9

====

.. codeauthor: Justyn Chaykowski <jbaergeist@gmail.com>

:Author Name: Justyn Chaykowski
:Author Email: jbaergeist@gmail.com

:Contributed versions:
   #. V0.01
   #. V0.1
   #. V0.2

====

.. vim: shiftwidth=3 tabstop=3:
