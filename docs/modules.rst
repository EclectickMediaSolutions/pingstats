Modules
=======

==================
pingstats.__init__
==================

.. automodule:: pingstats
.. autoclass:: pingstats.Pings
   :members: __init__
.. autofunction:: pingstats.get_pings
.. autofunction:: pingstats.plot_pings

============
pingstats.ui
============

.. automodule:: pingstats.ui

------------
Base Widgets
------------

.. autoclass:: pingstats.ui.Widget
   :members: __init__
.. autoclass:: pingstats.ui.PingWidget
   :members: __init__
.. autoclass:: pingstats.ui.PlotWidget
   :members: __init__

--------------
Status Widgets
--------------

.. autoclass:: pingstats.ui.RawStatus
.. autoclass:: pingstats.ui.AverageStatus
.. autoclass:: pingstats.ui.DroppedStatus
.. autoclass:: pingstats.ui.ProgStatus
.. autoclass:: pingstats.ui.RealtimeStatus

------------
Plot Widgets
------------

.. autoclass:: pingstats.ui.AveragePlot
.. autoclass:: pingstats.ui.RealtimePlot

------------
Pane Widgets
------------

.. autoclass:: pingstats.ui.AveragePane
.. autoclass:: pingstats.ui.RealtimePane

------------------------
Creating your own Widget
------------------------

To create your own widget, simply inherit from the topmost relevant widget class,
override the init class, and then define your output to the ``stdout``. For
example:

.. include:: examples/custom_widget.py
  :code: python


.. warning:: Arguments are passed to the widget classes internally. You can
             mirror the arguments of the parent class init function, or pass
             ``*args``
.. note:: The runner class uses the name of the widget to determine how much space
          each should take on screen, where status widgets take one line and
          plot/pane widgets take *n* lines. Therefore, you should specify what
          kind of widget it is in the object name.


==================
User Configuration
==================

.. automodule:: pingstats.config
