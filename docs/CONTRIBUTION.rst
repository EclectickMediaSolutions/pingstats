.. Outlines contribution guidelines

.. toctree::
   :maxdepth: 3

===========================
Contributing to the Project
===========================
-------------------
Forking the project
-------------------

To fork the project, first clone it from `the official repositry`_:
::

  git clone git@gitlab.com:EclectickMediaSolutions/pingstats.git

Then, create your own public repository and set the origin url to that repository:
::

  git remote set-url origin *your repository*

Once we have set your remote to origin, we can tell git where the upstream is:
::

  git remote add upstream git@gitlab.com:EclectickMediaSolutions/pingstats.git

.. note:: The upstream should be the official repository.
.. note:: For more on the git native forking workflow, see the `git forking
          tutorial`_

-----------------
Our issue tracker
-----------------

The project uses `git bug`_ to track issues. To pull our issues, first install
the issue tracker then pull issues from `the official repository`_:
::

  git bug pull upstream

You can then use the git bug termui to modify bugs:
::

  git bug termui

.. note:: Please consult `git bug`_'s documentation for more on how to use the
          software

Once you have created a new bug or modified an existing bug, push your changes
upstream:
::

  git bug push upstream

--------------------------
Git pull request structure
--------------------------

To submit a pull request, create a new comment on the respective bug in the issue
tracker with the output of the following git command:
::

  git request-pull upstream/master origin

.. note:: To generate a report for a specific branch, include that branch name
          after origin

.. _`the official repository`: https://gitlab.com/EclectickMediaSolutions/pingstats
.. _`git forking tutorial`: https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow
.. _`our gitlab`: https://gitlab.com/eclectickmediasolutions/pingstats
.. _`git bug`: https://github.com/MichaelMure/git-bug
