from pingstats.ui import Runner, PingWidget  # Import resources


class MyStatus(PingWidget):  # Define our widget
    def __init__(self, *args):  # Overide init, collecting any args
        super(MyStatus, self).__init__(*args)  # Pass args to the original function
        print('hello world')  # Perform output


if __name__ == '__main__':
    # instantiate runner, pass a list containing our Widget and the equivalent of sys.argv
    runner = Runner([MyStatus], ['-l', 'droppedstatus,mystatus,realtimestatus', 'google.ca'])
    runner()  # Run logic loop
