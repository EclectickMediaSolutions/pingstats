.. pingstats documentation master file, created by
   sphinx-quickstart on Thu Jan 10 00:58:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pingstats's documentation!
=====================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   README.rst
   AUTHORS.rst
   CHANGELOG.rst
   modules.rst
   LICENSE.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
