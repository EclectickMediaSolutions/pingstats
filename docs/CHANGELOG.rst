.. Changelog first added at V0.2

.. toctree::
   :maxdepth: 2

.. sectionauthor: Ariana Giroux <ariana.giroux@gmail.com>
.. _change-log:

Change log
==========

Outlines top level talking points of each major version change.

====

**V0.1**

 - Initial functionality
 - (tag: V0.1) Added some basic docs

**V0.2**

 - (tag: V0.1) Added some basic docs
 - Cleaned up formatting on README.rst
 - Updated git submodule listings
 - Implemented command line argument
 - Updated docs for script
 - (tag: V0.2) Implemented pip safe setuputils install

**V0.3**

 - (tag: V0.2) Implemented pip safe setuputils install
 - Added automated install script used by wget pipe
 - Made install script prettier
 - Made install script remove source after install
 - (tag: V0.3) Updated install instructions to specify WGET method

**V0.4**

 - (tag: V0.3) Updated install instructions to specify WGET method
 - (tag: V0.3.1) Fixed install instructions
 - Made screen update logic smarter
 - Made keyboard interrupt cleaner
 - (tag: V0.3.2) Updated versioning
 - (tag: V0.3.3) Fixed a bug with versioning
 - (tag: V0.3.4) Made graph scale to terminal size
 - updated readme with usage examples
 - Added an authors file
 - Made install script handle git init
 - Scaled down total packets displayed, fixed bug
 - (tag: V0.3.5) Made script actually display lost packets
 - Fixed UI scaling to better fit terminals
 - Added a status bar
 - Updated setup and authors to 0.4
 - (tag: V0.4) Merge branch 'master' of gitlab.com:EclectickMediaSolutions/pingstats
 - Added classifiers for pypi
 - (tag: V0.4.1) Updated install instructions for pip
 - Made setup grab readme
 - (tag: V0.4.2) Updated documentation, fixed packaging bugs
 - (tag: V0.4.3) Updated README for legacy install instructions


**V0.5**

 - Made install script handle git init
 - (tag: V0.4) Merge branch 'master' of gitlab.com:EclectickMediaSolutions/pingstats
 - Added classifiers for pypi
 - (tag: V0.4.1) Updated install instructions for pip
 - Made setup grab readme
 - (tag: V0.4.2) Updated documentation, fixed packaging bugs
 - (tag: V0.4.3) Updated README for legacy install instructions
 - (tag: V0.5a1) Changed hipsterplot dependency behaviour, fits pip
 - (tag: V0.5b1) Made software display additional plot with average
 - Fixed some display bugs from V0.5b2
 - Docs now reference new functionality, version
 - (tag: V0.5b2) Updated Authors
 - Updated the way pypi gets version number
 - Made setup.py call pytest
 - Wrote some basic tests
 - Added version to middle line
 - Merge branch 'tests'
 - Added setup.cfg alias for pytest
 - (tag: V0.5b3) Added tests instructions, updated readme image
 - (tag: V0.5) Graduate V0.5b3 to V0.5, add changelog

**V0.6**

 - (tag: V0.5) Graduate V0.5b3 to V0.5, add changelog
 - Made getpings handle nt search constraints
 - Added nt safe Popen mechanics, nt ping grepping
 - (tag: V0.6) Updated to version 0.6
 - (tag: V0.6.1) Removed errant print statements
 - (tag: V0.6.1.1) Fixed versioning
 - (tag: V0.6.2) Replace _get_tty with  stdlib get_terminal_size


**V0.7**

 - (tag: V0.6) Updated to version 0.6
 - (tag: V0.6.1) Removed errant print statements
 - (tag: V0.6.1.1) Fixed versioning
 - (tag: V0.6.2) Replace _get_tty with  stdlib get_terminal_size
 - (tag: V0.7) Added a more sane access approach

**V0.8**

 - (tag: V0.7) Added a more sane access approach
 - (tag: V0.8) Improves API structure, adds minor features
 - (tag: V0.8.1) Re-implement list constructors, rename args


**V0.9**

 - (tag: V0.8.1) Re-implement list constructors, rename args
 - (tag: V0.8.1.1) Fixed distultils binary pointer
 - Made pingstats.get_pings modular
 - Added tests for new pings object
 - Updated global scale values to better scale ui
 - Made pingstats.Pings object data names sane
 - Re-implemented pingstats.get_pings
 - Added deprecated calls to pingstats._run
 - Finished implementing tests f/ logic
 - (tag: V0.9b1) Re-implemented pingstats._run for legacy support
 - Added tests f/ UI
 - Made pingstats.get_pings more clear
 - Updated documentation f/ project
 - Added new docs via sphinx
 - Removed old docs
 - Updated changelog and authors f/ V0.9
 - (tag: V0.9b2) Added dynamic ui layout manipulation
 - Updated version tag
 - Merge remote-tracking branch 'internal/0.9b3-docs'
 - Added a basic readme back to the project
 - (tag: V0.9b3) Fixed readme naming....
 - Fixed Justyn Chaykowski author credit
 - (tag: V0.9b3.1) Various bugfixes, display fixes
 - Added git hooks and configured repo hooks path
 - Added a setup script for git hooks
 - Removed old setup.sh
 - Updated docs with hook install information
 - Updated base readme
 - Moved git hooks to more visible location
 - (tag: V0.9b3.2) Sanitizes plot/widget sizing.
 - Sanitizes plot/widget sizing.
 - (tag: V0.9b4) Fix versioning
 - Added status line, displays most recent raw data
 - Adds tests for commit c93afd9
 - Fixes scaling for windows environments
 - Added a dropped packets status widget
 - Extended DroppedStatus with docs and more functionality
 - Added tests for DroppedStatus
 - Adds documentation to RawStatus
 - Updated tests to be more NT aware
 - Merge remote-tracking branch 'internal/raw_status'
 - Merge remote-tracking branch 'internal/dropped_status'
 - (tag: V0.9b5) Added new status widgets, fixes windows scaling
 - Updated docs to represent new changes
 - Removed deprecated code (was deprecated in V0.8)
 - Cleaned build directory for readthedocs
 - Removed sub module
 - (tag: V0.9) Merge remote-tracking branches 'internal/0.9-updated-docs' and 'internal/cleanup-deprecated'
 - (tag: V0.9.1) Fix versioning... again
 - Made widget object names more sane.
 - Reimplement clear on ui loop
 - Merge branches 'fix-ui-naming' and 'reimplement-clear' into rolling-release
 - (tag: V0.9.2b1) Updated to V0.9.2b1 on rolling release
 - Prune references to git tasks
 - Merge branch 'prune_git_tasks' into rolling-release
 - Cleans up changelog, adds changelog generation script
 - Merge branch 'prune-changelog' into rolling-release
 - Added example output to all status widget docstrings
 - Merge branch 'widget-example-docstrings' into rolling-release
 - Updated module autodocs to be more legible/sensible
 - Updated Readme software description
 - Removed scripts for old git tasks system
 - Merge branches 'update_automodule', 'update_readme_description' and 'update-hooks' into rolling-release
 - (tag: V0.9.2b2) Update to V0.9.2b2
 - (tag: V0.9.2, origin/master, master) Update version to reflect V0.9.2b2 passing all tests
 - Added config parsing capabilities via config.py
 - Added config API to __init__.py
 - Fixed a bug for nt
 - Added some basic docs for config.py
 - Added config.py docs to sphinx-docs
 - Removed default docs theme to enable Readthedocs defaults
 - Merge remote-tracking branch 'origin/user-preference-file' into rolling-release
 - Added install instructions for installing from source
 - Added an interface to provide ui.Runner with user derived widgets
 - Remove reference to nix only installs
 - Added long-description back to setup.py
 - Added command line arguments to README
 - Added a "widgets" property to ui.Runner to make things cleaner
 - Updated tests to remove warnings from pytest
 - (tag: V0.9.3) Added basic tests to ui.Runner
 - Update versioning
 - (tag: V0.9.3.1) Ensured Runner executed from setuputils
 - Updated version
 - Updated contribution guidelines to outline git bug workflow
 - Fixed argv pass to run function
 - Added tests for runtime and parser
 - Added test to ensure path is set properly f/ console scripts
 - (tag: V0.9.4) Moved custom_widget snippet to example file

